package content

import (
	"fmt"
	"github.com/bosssauce/reference"
	"github.com/ponzu-cms/ponzu/management/editor"
	"github.com/ponzu-cms/ponzu/system/item"
)

type Pages struct {
	item.Item

	Title    	  			  string  	`json:"title"`
	Content  	  			  string  	`json:"content"`
	Order	 	  			  string 	`json:"order"`
	SeoTitle 	  	  		  string    `json:"seo_title"`
	SEO      	  	  	  	[]string  	`json:"seo"`
	SeoDescription 	  		  string    `json:"seo_description"`
	SeoImage       	  		  string 	`json:"seo_image"`
	Visible  	  			  bool 	 	`json:"visible"`
	Image   	  			  string  	`json:"image"`
	Url           			  string  	`json:"url"`
	Categories    			  string 	`json:"categories"`
	Template 	  			  string  	`json:"template"`
	SubCategories 			[]string 	`json:"subcategories"`
	// Home struct
	HomeAdvantages 			[]string  	`json:"home_advantages"`
	HomeRecentDescription 	  string  	`json:"home_description"`
	HomePartners  			[]string  	`json:"home_partners"`
	HomeTestimonials  		[]string  	`json:"home_testimonials"`
	HomeItcPriceOption		[]string	`json:"home_itc_price_option"`
	HomeItcPriceValue		[]string	`json:"home_itc_price_value"`
	HomeItcImage			  string	`json:"home_itc_image"`
	HomeItcDescription		  string	`json:"home_itc_description"`
	HomeItcLink				  string	`json:"home_itc_link"`
	// Product Elements
	ProductsDescription 	  string  	`json:"products_description"`
	ProductsImage			  string	`json:"products_image"`
	// About Struct
	AboutDescription	 	  string  	`json:"about_description"`
	AboutCertificates		[]string	`json:"about_certificates"`
	AboutInfo				[]string	`json:"about_info"`
	AboutPartners			[]string	`json:"about_partners"`
	AboutImage				  string	`json:"about_image"`
	AboutJS				  	  string	`json:"about_js"`

}


// type Custom struct {}

// MarshalEditor writes a buffer of html to edit a Pages within the CMS
// and implements editor.Editable
func (p *Pages,) MarshalEditor() ([]byte, error) {
// creating 
// attempting to add custom fields
var fields []editor.Field
	switch p.Template {
	case "home":
		fields = []editor.Field{
		//
		// Main Customs
		//
		{
			View: []byte(`
				<div class="input-field col s12">
					<h5 class="active">Настройки шаблона главной страницы</h5>
				</div>
				`),
		},
		{
			View: editor.InputRepeater("HomeAdvantages", p, map[string]string{
			"label":       "Преимущества",
			"type":        "text",
			"placeholder": "Введите данные",
		}),},
		{
			View: editor.Richtext("HomeRecentDescription", p, map[string]string{
			"label":       "Описание в Популярных *",
			"type":        "text",
			"placeholder": "Введите текст",
		}),},
		//
		// ITC Customs
		//
		{
			View: []byte(`
				<div class="input-field col s12">
					<h5 class="active">Настройки блока ИТС</h5>
				</div>
				`),
		},
		{
			View: editor.InputRepeater("HomeItcPriceOption", p, map[string]string{
			"label":       "Название пакета",
			"type":        "text",
			"placeholder": "Введите данные",
		}),},
		{
			View: editor.InputRepeater("HomeItcPriceValue", p, map[string]string{
			"label":       "Цена пакета пакета",
			"type":        "text",
			"placeholder": "Введите данные",
		}),},
		{
			View: editor.File("HomeItcImage", p, map[string]string{
			"label":       "Картинка блока ИТС",
			"type":        "text",
			"placeholder": "Введите данные",
		}),},
		{
			View: editor.Richtext("HomeItcDescription", p, map[string]string{
			"label":       "Описание Блока ИТС",
			"type":        "text",
			"placeholder": "Введите текст",
		}),},
		{
			View: editor.InputRepeater("HomeItcLink", p, map[string]string{
			"label":       "Ссылка кнопки подробнее",
			"type":        "text",
			"placeholder": "Введите данные",
		}),},
		//
		// Partners Customs
		//
		{
			View: []byte(`
				<div class="input-field col s12">
					<h5 class="active">Настройки блока партнеров</h5>
				</div>
				`),
		},
		{
			View: editor.FileRepeater("HomePartners", p, map[string]string{
			"label":       "Загрузите логотипы партнеров",
			"type":        "text",
			"placeholder": "Введите данные",
		}),},
		{
			View: editor.InputRepeater("HomeTestimonials", p, map[string]string{
			"label":       "Загрузите отзывы партнеров",
			"type":        "text",
			"placeholder": "Введите данные",
		}),}}
		// 
		// ABOUT PAGE
		// 
	case "about":
		fields = []editor.Field{ {
			View: []byte(`
				<div class="input-field col s12">
					<h5 class="active">Настройки шаблона О НАС</h5>
				</div>
				`),
		},
		{
			View: editor.Richtext("AboutDescription", p, map[string]string{
				"label":       "Описание Блока О нас",
				"type":        "text",
				"placeholder": "Введите текст",
			}),
		},
		{
			View: editor.FileRepeater("AboutCertificates", p, map[string]string{
				"label":       "Загрузите Сертификаты",
				"type":        "text",
				"placeholder": "Введите данные",
			}),
		},
		{
			View: editor.FileRepeater("AboutPartners", p, map[string]string{
				"label":       "Загрузите логотипы партнеров",
				"type":        "text",
				"placeholder": "Введите данные",
			}),
		},
		{
			View: editor.InputRepeater("AboutInfo", p, map[string]string{
				"label":       "Реквизиты",
				"type":        "text",
				"placeholder": "Введите данные",
			}),
		},
		{
			View: editor.Textarea("AboutJS", p, map[string]string{
				"label":       "Вставьте код Карты",
				"type":        "text",
				"placeholder": "Введите текст",
			}),
		},
		{
			View: editor.File("AboutImage", p, map[string]string{
				"label":       "Картинка О нас",
				"type":        "text",
				"placeholder": "Введите данные",
			}),
		},{},{},{},{},{} }
	case "products":
		fields = []editor.Field{ {
			View: []byte(`
				<div class="input-field col s12">
					<h5 class="active">Настройки шаблона КАССЫ/ПРОДУКТЫ</h5>
				</div>
				`),
		},
		{			
			View: editor.Richtext("ProductsDescription", p, map[string]string{
			"label":       "Описание Страницы",
			"type":        "text",
			"placeholder": "Введите текст",
		}),
		},
		{
			View: editor.File("ProductsImage", p, map[string]string{
				"label":       "Изображение Страницы",
				"type":        "text",
				"placeholder": "Введите данные",
			}),
		},
		{},{},{},{},{},{},{},{},{} }
	case "cashboxes":
		fields = []editor.Field{ {
			View: []byte(`
				<div class="input-field col s12">
					<h5 class="active">Настройки шаблона Онлайн-кассы</h5>
				</div>
				`),
		},		{			
			View: editor.Richtext("ProductsDescription", p, map[string]string{
			"label":       "Описание Страницы",
			"type":        "text",
			"placeholder": "Введите текст",
		}),
		},
		{
			View: editor.File("ProductsImage", p, map[string]string{
				"label":       "Изображение Страницы",
				"type":        "text",
				"placeholder": "Введите данные",
			}),
		},{},{},{},{},{},{},{},{},{} }
	case "services":
		fields = []editor.Field{ {
			View: []byte(`
				<div class="input-field col s12">
					<h5 class="active">Настройки шаблона страницы Услуг</h5>
				</div>
				`),
		},		{			
			View: editor.Richtext("ProductsDescription", p, map[string]string{
			"label":       "Описание Страницы",
			"type":        "text",
			"placeholder": "Введите текст",
		}),
		},
		{
			View: editor.File("ProductsImage", p, map[string]string{
				"label":       "Изображение Страницы",
				"type":        "text",
				"placeholder": "Введите данные",
			}),
		},{},{},{},{},{},{},{},{},{} }
	case "contacts":
		fields = []editor.Field{ {
			View: []byte(`
				<div class="input-field col s12">
					<h5 class="active">Настройки шаблона страницы Контакты</h5>
				</div>
				`),
		},{},{},{},{},{},{},{},{},{},{},{} }
	case "news":
		fields = []editor.Field{ {
			View: []byte(`
				<div class="input-field col s12">
					<h5 class="active">Настройки шаблона страницы Новостей</h5>
				</div>
				`),
		},		{			
			View: editor.Richtext("ProductsDescription", p, map[string]string{
			"label":       "Описание Страницы",
			"type":        "text",
			"placeholder": "Введите текст",
		}),
		},
		{
			View: editor.File("ProductsImage", p, map[string]string{
				"label":       "Изображение Страницы",
				"type":        "text",
				"placeholder": "Введите данные",
			}),
		},{},{},{},{},{},{},{},{},{} }
	default:
		fields = []editor.Field{ {},{},{},{},{},{},{},{},{},{},{},{} }
	}
	view, err := editor.Form(p,
		// Take note that the first argument to these Input-like functions
		// is the string version of each Pages field, and must follow
		// this pattern for auto-decoding and auto-encoding reasons:
		editor.Field{
			View: editor.Input("Title", p, map[string]string{
				"label":       "Заголовок страницы",
				"type":        "text",
				"placeholder": "Введите данные",
			}),
		},
		editor.Field{
			View: editor.Input("Order", p, map[string]string{
				"label":       "Порядковый номер",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Select("Template", p, map[string]string{
				"label": "Шаблон",
			}, map[string]string{
				"home" : "Главная",
				"about" : "О нас",
				"products" : "Продукты",
				"cashboxes" : "Онлайн-кассы",
				"services" : "Услуги",
				"contacts" : "Контакты",
				"news" : "Новости",
			}),
		},
		fields[0],fields[1],fields[2],fields[3],fields[4],fields[5],fields[6],fields[7],fields[8],fields[9],fields[10],fields[11],
		editor.Field{
			View: []byte(`
				<div class="input-field col s12">
					<h5 class="active">Основные настройки</h5>
				</div>
				`),
		},
		editor.Field{
			View: reference.SelectRepeater("SubCategories", p, map[string]string{
				"label": "Подкатегории",
			},
				"Категории",
				`{{ .name }}`,
			),
		},
		editor.Field{
			View: editor.Tags("SEO", p, map[string]string{
				"label":       "SEO Ключевые слова",
				"placeholder": "+Ключевые слова",
			}),
		},
		editor.Field{
			View: editor.Input("SeoTitle", p, map[string]string{
				"label":       "Seo Заголовок",
				"type":        "text",
				"placeholder": "Введите описание",
			}),
		},
		editor.Field{
			View: editor.Input("SeoDescription", p, map[string]string{
				"label":       "Seo Описание",
				"type":        "text",
				"placeholder": "Введите описание",
			}),
		},
		editor.Field{
			View: editor.File("SeoImage", p, map[string]string{
				"label":       "Изображение SEO",
				"placeholder": "Загрузите изображение",
			}),
		},
		editor.Field{
			View: editor.Select("Visible", p, map[string]string{
				"label": "Видимость",
			}, map[string]string{
				// "value": "Display Name",
				"true" : "Видимая",
				"false" : "Невидимая",
			}),
		},
		editor.Field{
			View: editor.File("Image", p, map[string]string{
				"label":       "Seo Изображение",
				"placeholder": "Введите изображение",
			}),
		},
		
		editor.Field{
			View: editor.Input("Url", p, map[string]string{
				"label":       "Url",
				"type":        "text",
				"placeholder": "Введите ссылку на страницу",
			}),
		},

	)

	if err != nil {
		return nil, fmt.Errorf("Failed to render Pages editor view: %s", err.Error())
	}
	return view, nil
}

func init() {
	item.Types["Страницы"] = func() interface{} { return new(Pages) }
}

// String defines how a Pages is printed. Update it using more descriptive
// fields from the Pages struct type
func (p *Pages) String() string {
	return fmt.Sprintf(p.Title)
}
