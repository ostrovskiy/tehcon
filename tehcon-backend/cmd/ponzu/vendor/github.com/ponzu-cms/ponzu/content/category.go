package content

import (
	"fmt"
	"github.com/bosssauce/reference"
	"github.com/ponzu-cms/ponzu/management/editor"
	"github.com/ponzu-cms/ponzu/system/item"
)

type Category struct {
	item.Item

	Name             string 		`json:"name"`
	Image            string 		`json:"image"`
	Subcategories  []string 		`json:"subcategories"`
	Visible     	 bool			`json:"visible"`
	Showinparent   	 bool			`json:"show_in_parent"`
}

// MarshalEditor writes a buffer of html to edit a Category within the CMS
// and implements editor.Editable
func (c *Category) MarshalEditor() ([]byte, error) {
	view, err := editor.Form(c,
		// Take note that the first argument to these Input-like functions
		// is the string version of each Category field, and must follow
		// this pattern for auto-decoding and auto-encoding reasons:
		editor.Field{
			View: editor.Input("Name", c, map[string]string{
				"label":       "Имя Категории",
				"type":        "text",
				"placeholder": "Введите имя категории",
			}),
		},
		editor.Field{
			View: editor.Select("Visible", c, map[string]string{
				"label": "Видимость в меню категорий",
			}, map[string]string{
				// "value": "Display Name",
				"true" : "Видимая",
				"false" : "Невидимая", 
			}),
		},
		editor.Field{
			View: editor.Select("Showinparent", c, map[string]string{
				"label": "Видимость Продуктов в родительских категориях",
			}, map[string]string{
				// "value": "Display Name",
				"true" : "Видимая",
				"false" : "Невидимая", 
			}),
		},
		editor.Field{
			View: editor.File("Image", c, map[string]string{
				"label":       "Изображение",
				"placeholder": "Загрузите ваше изображение",
			}),
		},
		editor.Field{
			View: reference.SelectRepeater("Subcategories", c, map[string]string{
				"label": "Подкатегория",
			},
				"Категории",
				`{{ .name }}`,
			),
		},
	)

	if err != nil {
		return nil, fmt.Errorf("Failed to render Category editor view: %s", err.Error())
	}

	return view, nil
}

func init() {
	item.Types["Категории"] = func() interface{} { return new(Category) }
}

func (c *Category) String() string {
	return fmt.Sprintf(c.Name)
}
