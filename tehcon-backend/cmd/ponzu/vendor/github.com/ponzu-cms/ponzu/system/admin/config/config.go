// Package config provides a content type to manage the Ponzu system's configuration
// settings for things such as its name, domain, HTTP(s) port, email, server defaults
// and backups.
package config

import (
	"github.com/ponzu-cms/ponzu/management/editor"
	"github.com/ponzu-cms/ponzu/system/item"
)

// Config represents the confirgurable options of the system
type Config struct {
	item.Item

	Name                    string   `json:"name"`
	Domain                  string   `json:"domain"`
	BindAddress             string   `json:"bind_addr"`
	HTTPPort                string   `json:"http_port"`
	HTTPSPort               string   `json:"https_port"`
	// Mailer Addon
	AdminEmail              string   `json:"admin_email"`
	SMTPServer              string   `json:"smtp_server"`
	SMTPPort                string   `json:"smtp_port"`
	SMTPPassword            string   `json:"smtp_password"`
	MailFrom                string   `json:"mail_from"`
	// Mailer Addon
	ClientSecret            string   `json:"client_secret"`
	Etag                    string   `json:"etag"`
	DisableCORS             bool     `json:"cors_disabled"`
	DisableGZIP             bool     `json:"gzip_disabled"`
	DisableHTTPCache        bool     `json:"cache_disabled"`
	CacheMaxAge             int64    `json:"cache_max_age"`
	CacheInvalidate         []string `json:"cache"`
	BackupBasicAuthUser     string   `json:"backup_basic_auth_user"`
	BackupBasicAuthPassword string   `json:"backup_basic_auth_password"`
}

const (
	dbBackupInfo = `
		<p class="flow-text">Database Backup Credentials:</p>
		<p>Add a user name and password to download a backup of your data via HTTP.</p>
	`
)

// String partially implements item.Identifiable and overrides Item's String()
func (c *Config) String() string { return c.Name }

// MarshalEditor writes a buffer of html to edit a Post and partially implements editor.Editable
func (c *Config) MarshalEditor() ([]byte, error) {
	view, err := editor.Form(c,
		editor.Field{
			View: editor.Input("Name", c, map[string]string{
				"label":       "Имя сайта",
				"placeholder": "Добавьте имя для админ панели",
			}),
		},
		editor.Field{
			View: editor.Input("Domain", c, map[string]string{
				"label":       "Доменное Имя (нужно для настройки CORS)",
				"placeholder": "напр. www.example.com или example.com",
			}),
		},
		editor.Field{
			View: editor.Input("BindAddress", c, map[string]string{
				"type": "hidden",
			}),
		},
		editor.Field{
			View: editor.Input("HTTPPort", c, map[string]string{
				"type": "hidden",
			}),
		},
		editor.Field{
			View: editor.Input("HTTPSPort", c, map[string]string{
				"type": "hidden",
			}),
		},
		editor.Field{
			View: []byte(`
            <div class="input-field col s12">
                <h5 class="active">Настройки Cold's SMTP Mailer</h5>
            </div>
            `),
		},
		editor.Field{
			View: editor.Input("AdminEmail", c, map[string]string{
				"label": "Имеил администратора для уведомлений",
			}),
		},
		editor.Field{
			View: editor.Input("MailFrom", c, map[string]string{
				"label":       "Email От кого(SMTP Пользователь)",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("SMTPPassword", c, map[string]string{
				"label":       "SMTP Пароль",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("SMTPServer", c, map[string]string{
				"label":       "Адрес SMTP сервера",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("SMTPPort", c, map[string]string{
				"label":       "SMTP порт",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: []byte(`
            <div class="input-field col s12">
                <h5 class="active">Настройки</h5>
            </div>
            `),
		},
		editor.Field{
			View: editor.Input("ClientSecret", c, map[string]string{
				"label":    "Client Secret (used to validate requests, DO NOT SHARE)",
				"disabled": "true",
			}),
		},
		editor.Field{
			View: editor.Input("ClientSecret", c, map[string]string{
				"type": "hidden",
			}),
		},
		editor.Field{
			View: editor.Input("Etag", c, map[string]string{
				"label":    "Etag Header (used to cache resources)",
				"disabled": "true",
			}),
		},
		editor.Field{
			View: editor.Input("Etag", c, map[string]string{
				"type": "hidden",
			}),
		},
		editor.Field{
			View: editor.Checkbox("DisableCORS", c, map[string]string{
				"label": "Выключить Корс (будет доступен для " + c.Domain + ")",
			}, map[string]string{
				"true": "Выключить CORS",
			}),
		},
		editor.Field{
			View: editor.Checkbox("DisableGZIP", c, map[string]string{
				"label": "Выключитьб сжатие GZIP (will increase server speed, but also bandwidth)",
			}, map[string]string{
				"true": "Выключить GZIP",
			}),
		},
		editor.Field{
			View: editor.Checkbox("DisableHTTPCache", c, map[string]string{
				"label": "Выключить HTTP Кэш (overrides 'Cache-Control' header)",
			}, map[string]string{
				"true": "Выключить HTTP Кэш",
			}),
		},
		editor.Field{
			View: editor.Input("CacheMaxAge", c, map[string]string{
				"label": "Max-Age value for HTTP caching (in seconds, 0 = 2592000)",
				"type":  "text",
			}),
		},
		editor.Field{
			View: editor.Checkbox("CacheInvalidate", c, map[string]string{
				"label": "Сбросить кэш",
			}, map[string]string{
				"invalidate": "Сбросить кэш",
			}),
		},
		editor.Field{
			View: []byte(dbBackupInfo),
		},
		editor.Field{
			View: editor.Input("BackupBasicAuthUser", c, map[string]string{
				"label":       "HTTP Basic Auth Пользователь",
				"placeholder": "Введите пользователя для загрузки дампов",
				"type":        "text",
			}),
		},
		editor.Field{
			View: editor.Input("BackupBasicAuthPassword", c, map[string]string{
				"label":       "HTTP Basic Auth Пароль",
				"placeholder": "Введите пароль для загрузки дампов",
				"type":        "password",
			}),
		},
	)
	if err != nil {
		return nil, err
	}

	open := []byte(`
	<div class="card">
		<div class="card-content">
			<div class="card-title">Настройки системы</div>
		</div>
		<form action="/admin/configure" method="post">
	`)
	close := []byte(`</form></div>`)
	script := []byte(`
	<script>
		$(function() {
			// hide default fields & labels unnecessary for the config
			var fields = $('.default-fields');
			fields.css('position', 'relative');
			fields.find('input:not([type=submit])').remove();
			fields.find('label').remove();
			fields.find('button').css({
				position: 'absolute',
				top: '-10px',
				right: '0px'
			});

			var contentOnly = $('.content-only.__ponzu');
			contentOnly.hide();
			contentOnly.find('input, textarea, select').attr('name', '');

			// adjust layout of td so save button is in same location as usual
			fields.find('td').css('float', 'right');

			// stop some fixed config settings from being modified
			fields.find('input[name=client_secret]').attr('name', '');
		});
	</script>
	`)

	view = append(open, view...)
	view = append(view, close...)
	view = append(view, script...)

	return view, nil
}
