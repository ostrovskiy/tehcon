// Package config provides a content type to manage the Ponzu system's configuration
// settings for things such as its name, domain, HTTP(s) port, email, server defaults
// and backups.
package website

import (
	"github.com/ponzu-cms/ponzu/management/editor"
	"github.com/ponzu-cms/ponzu/system/item"
)

// Config represents the confirgurable options of the system
type Config struct {
	item.Item
	// Main info
	Logo                      string   `json:"logo"`
	Name                      string   `json:"name"`
	Address                   string   `json:"address"`
	Time                   	  string   `json:"time"`
	Email                     string   `json:"email"`
	Phone                     string   `json:"phone"`
	Meta                      string   `json:"meta"`
	MetaKeywords			[]string   `json:"metakeywords"`
	// Footer
	FooterLogo				  string   `json:"footer_logo"`
	FooterInfo				  string   `json:"footer_info"`
	FooterCopyrights		  string   `json:"footer_copyrights"`
	// Social
	Facebook			  	  string   `json:"social_facebook"`
	Instagram			  	  string   `json:"social_instagram"`
	Telegram			  	  string   `json:"social_telegram"`
	Vk					  	  string   `json:"social_vk"`
	Viber				  	  string   `json:"social_viber"`
	//Js
	CustomJs                  string   `json:"custom_js"`
	// Emailing
	SMTPServer                string   `json:"smtp_server"`
	SMTPEncryption            string   `json:"smtp_encryption"`
	SMTPPort                  string   `json:"smtp_port"`
	SMTPUser                  string   `json:"smtp_user"`
	SMTPPassword              string   `json:"smtp_password"`
	EmailTemplate             string   `json:"email_template"`
	MailFrom                  string   `json:"mail_from"`
	MailTo                    string   `json:"mail_to"`

}

// String partially implements item.Identifiable and overrides Item's String()
func (c *Config) String() string { return c.Logo }

// MarshalEditor writes a buffer of html to edit a Post and partially implements editor.Editable
func (c *Config) MarshalEditor() ([]byte, error) {
	view, err := editor.Form(c,
		editor.Field{
			View: []byte(`
            <div class="input-field col s12">
                <h5 class="active">Основные</h5>
            </div>
            `),
		},
		editor.Field{
			View: editor.Input("Logo", c, map[string]string{
				"label":       "Логотип",
				"placeholder": "Вставьте ссылку на логотип",
			}),
		},
		editor.Field{
			View: editor.Input("Name", c, map[string]string{
				"label":       "Название Вебсайта",
				"placeholder": "Введдите имя",
			}),
		},
		editor.Field{
			View: editor.Input("Address", c, map[string]string{
				"label":       "Адрес",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("Phone", c, map[string]string{
				"label":       "Номер телефона",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("Time", c, map[string]string{
				"label":       "Рабочее время",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("Email", c, map[string]string{
				"label":       "Email для связи",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: []byte(`
            <div class="input-field col s12">
                <h5 class="active">Настройки Seo</h5>
            </div>
            `),
		},
		editor.Field{
			View: editor.Input("Meta", c, map[string]string{
				"label":       "Meta Описание сайта",
				"type":        "text",
				"placeholder": " ",
			}),
		},
		editor.Field{
			View: editor.Tags("MetaKeywords", c, map[string]string{
				"label":       "Meta Ключевые слова",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: []byte(`
            <div class="input-field col s12">
                <h5 class="active">Настройки Футера</h5>
            </div>
            `),
		},
		editor.Field{
			View: editor.Input("FooterLogo", c, map[string]string{
				"label":       "Логотип в футере",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: []byte(`
            <div class="input-field col s12">
                <h5 class="active">Настройки Соициальных сетей</h5>
            </div>
            `),
		},
		editor.Field{
			View: editor.Input("Facebook", c, map[string]string{
				"label":       "Cсылка на Facebook",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("Instagram", c, map[string]string{
				"label":       "Cсылка на Instagram",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("Telegram", c, map[string]string{
				"label":       "Cсылка на Telegram",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("Vk", c, map[string]string{
				"label":       "Cсылка Vkontakte",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("Viber", c, map[string]string{
				"label":       "Cсылка Viber",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Richtext("FooterInfo", c, map[string]string{
				"label":       "Футер информация о нас",
				"placeholder": "Информация в блоке футер",
			}),
		},
		editor.Field{
			View: editor.Richtext("FooterCopyrights", c, map[string]string{
				"label":       "Футер права сайта",
				"placeholder": "Например: все права защищены",
			}),
		},
		editor.Field{
			View: []byte(`
            <div class="input-field col s12">
                <h5 class="active">Javascript</h5>
            </div>
            `),
		},
		editor.Field{
			View: editor.Textarea("CustomJs", c, map[string]string{
				"label":       "CustomJs",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: []byte(`
            <div class="input-field col s12">
                <h5 class="active">Настройки почтового сервера</h5>
            </div>
            `),
		},
		editor.Field{
			View: editor.Input("MailTo", c, map[string]string{
				"label":       "Email Кому",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("MailFrom", c, map[string]string{
				"label":       "Email От кого",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("SMTPServer", c, map[string]string{
				"label":       "Адрес SMTP сервера",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Select("SMTPEncryption", c, map[string]string{
				"label": "Выберите тип шифрования",
			}, map[string]string{
				// "value": "Display Name",
				"ssl":    "SSL",
				"tls": "TLS",
			}),
		},
		editor.Field{
			View: editor.Input("SMTPPort", c, map[string]string{
				"label":       "SMTP порт",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("SMTPUser", c, map[string]string{
				"label":       "SMTP Пользователь",
				"type":        "text",
				"placeholder": "",
			}),
		},
		editor.Field{
			View: editor.Input("SMTPPassword", c, map[string]string{
				"label":       "SMTP Пароль (сначало нужно зашифровать)",
				"type":        "text",
				"placeholder": "",
			}),
		},
	)
	if err != nil {
		return nil, err
	}

	open := []byte(`
	<div class="card">
		<div class="card-content">
			<div class="card-title">Конфигурация вебсайта</div>
		</div>
		<form action="/admin/website" method="post">
	`)
	close := []byte(`</form></div>`)
	script := []byte(`
	<script>
		$(function() {
			// hide default fields & labels unnecessary for the config
			var fields = $('.default-fields');
			fields.css('position', 'relative');
			fields.find('input:not([type=submit])').remove();
			fields.find('label').remove();
			fields.find('button').css({
				position: 'absolute',
				top: '-10px',
				right: '0px'
			});

			var contentOnly = $('.content-only.__ponzu');
			contentOnly.hide();
			contentOnly.find('input, textarea, select').attr('name', '');

			// adjust layout of td so save button is in same location as usual
			fields.find('td').css('float', 'right');

			// stop some fixed config settings from being modified
			fields.find('input[name=client_secret]').attr('name', '');
		});
	</script>
	`)

	view = append(open, view...)
	view = append(view, close...)
	view = append(view, script...)

	return view, nil
}
