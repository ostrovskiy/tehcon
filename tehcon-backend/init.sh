#!/bin/bash
BLUE='\033[0;34m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

cd /go/src/tehcon-backend

printf "\n\n${RED}Start${NC} Backend Node\n\n"
printf "\n\n${GREEN}Build${NC} Backend Node\n\n"
ponzu build
printf "\n\n${BLUE}Starting${NC} server \n\n"
ponzu run --bind=0.0.0.0
