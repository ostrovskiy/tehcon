package content

import (
	"fmt"
	// "log"
	"net/http"
	"github.com/ponzu-cms/ponzu/management/editor"
	"github.com/ponzu-cms/ponzu/system/item"
	"github.com/ponzu-cms/ponzu/system/db"
	// "gitlab.com/ostrovskiy/tehcon/tehcon-backend/addons/gitlab.com/ostrovskiy/mailer"
	"tehcon-backend/addons/gitlab.com/ostrovskiy/mailer"
)

type Email struct {
	item.Item

	Name           string `json:"name"`
	Phone          string `json:"phone"`
	Email 		   string `json:"email"`
	Product 	   string `json:"product"`
	Package		   string `json:"package"`
	Price		   string `json:"price"`
}

// MarshalEditor writes a buffer of html to edit a Category within the CMS
// and implements editor.Editable
func (c *Email) MarshalEditor() ([]byte, error) {
	view, err := editor.Form(c,
		// Take note that the first argument to these Input-like functions
		// is the string version of each Category field, and must follow
		// this pattern for auto-decoding and auto-encoding reasons:
		editor.Field{
			View: []byte(`
					<div class="user-data">
					<h6>
						<i class="material-icons">assignment_ind</i>
						Данные о клиенте:
					</h6>
					<label class="active email-data" for="Имя">
						<i class="material-icons">recent_actors</i>
						<b>Поступил заказ от: </b> `+ c.Name +`
					</label>
					<label class="active email-data">
						<i class="material-icons">phone_android</i>
						<b>Номер телефона: </b> ` + c.Phone + `
					</label>
					<label class="active email-data">
						<i class="material-icons">mail_outline</i>
						<b>Email: </b> ` + c.Email + `
					</label>
					<h6>
						<i class="material-icons">shopping_basket</i>
						Данные о продукте:
					</h6>
					<label class="active email-data">
						<i class="material-icons">archive</i>
						<b>Продукт: </b> ` + c.Product + `
					</label>
					<label class="active email-data">
						<i class="material-icons">move_to_inbox</i>
						<b>Пакет: </b> ` + c.Package + `</label>
					<label class="active email-data">
						<i class="material-icons">attach_money</i>
						<b>Цена: </b> ` + c.Price + ` руб.</label>
					</div>
					`),
			},
	)

	if err != nil {
		return nil, fmt.Errorf("Failed to render Category editor view: %s", err.Error())
	}

	return view, nil
}

func init() {
	item.Types["Emails"] = func() interface{} { return new(Email) }
}

func (s *Email) Create(res http.ResponseWriter, req *http.Request) error {
	// do form data validation for required fields
	required := []string{
		"name",
		"phone",
		"product",
	}

	for _, r := range required {
		if req.PostFormValue(r) == "" {
			err := fmt.Errorf("request missing required field: %s", r)
			return err
		}
	}

	return nil
}

func (s *Email) BeforeAPICreate(res http.ResponseWriter, req *http.Request) error {
	// do initial user authentication here on the request, checking for a
	// token or cookie, or that certain form fields are set and valid

	// // for example, this will check if the request was made by a CMS admin user:
	// if !user.IsValid(req) {
	// 	return api.ErrNoAuth
	// }

	// you could then to data validation on the request post form, or do it in
	// the Create method, which is called after BeforeAPICreate

	return nil
}

// AfterAPICreate is called after Create, and is useful for logging or triggering
// notifications, etc. after the data is saved to the database, etc.
// The request has a context containing the databse 'target' affected by the
// request. Ex. Song__pending:3 or Song:8 depending if Song implements api.Trustable
func (s *Email) AfterAPICreate(res http.ResponseWriter, req *http.Request) error {
	data := make(map[string]interface{})
	user := make(map[string]interface{})
	// SMTPUSER
	data["mail_from"] = db.ConfigCache("mail_from")
	data["admin_email"] = db.ConfigCache("admin_email")
	data["smtp_server"] = db.ConfigCache("smtp_server")
	data["smtp_port"] = db.ConfigCache("smtp_port")
	data["smtp_password"] = db.ConfigCache("smtp_password")
	data["name"] = db.ConfigCache("name")
	// USER DATA
	user["name"] = s.Name
	user["phone"] = s.Phone
	user["email"] = s.Email
	user["product"] = s.Product
	user["package"] = s.Package
	user["price"] = s.Price
	// log.Println("\n\nData Mailform:",data["mail_from"])
	// log.Println("\n\nData Sitename:",data["name"])
	// log.Println("\n\nUser Name:",user["name"])
	// log.Println("\n\nUser Product:",user["product"])
	mailer.SendMsg(data, user)
	return nil
}

func (s *Email) Approve(res http.ResponseWriter, req *http.Request) error {
	return nil
}


// func (s *Email) AutoApprove(res http.ResponseWriter, req *http.Request) error {
// 	// Use AutoApprove to check for trust-specific headers or whitelisted IPs,
// 	// etc. Remember, you will not be able to Approve or Reject content that
// 	// is auto-approved. You could add a field to Song, i.e.
// 	// AutoApproved bool `json:auto_approved`
// 	// and set that data here, as it is called before the content is saved, but
// 	// after the BeforeSave hook.

// 	return nil
// }

func (c *Email) String() string {
	return fmt.Sprintf(c.Name)
}
