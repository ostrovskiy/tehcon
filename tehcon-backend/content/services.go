package content

import (
	"fmt"

	"github.com/bosssauce/reference"

	"github.com/ponzu-cms/ponzu/management/editor"
	"github.com/ponzu-cms/ponzu/system/item"
)

type Services struct {
	item.Item

	Name        	  string 	`json:"name"`
	Price       	  string 	`json:"price"`
	Sale	    	  string 	`json:"sale"`
	Category    	  string 	`json:"category"`
	PriceOption		[]string	`json:"price_option"`
	PriceValue		[]string	`json:"price_value"`
	Description 	  string 	`json:"description"`
	Image       	  string 	`json:"image"`
	SeoTitle 	  	  string    `json:"seo_title"`
	Tags	  		[]string 	`json:"tags"`
	SeoDescription 	  string    `json:"seo_description"`
	SeoImage       	  string 	`json:"seo_image"`
	Related   		[]string 	`json:"related_services"`
	Hot		     	  bool		`json:"hot"`
	Visible     	  bool		`json:"visible"`
	Recent			  bool	    `json:"recent"`
	RelatedProducts []string 	`json:"related_products"`
}

// MarshalEditor writes a buffer of html to edit a Services within the CMS
// and implements editor.Editable
func (s *Services) MarshalEditor() ([]byte, error) {
	view, err := editor.Form(s,
		// Take note that the first argument to these Input-like functions
		// is the string version of each Services field, and must follow
		// this pattern for auto-decoding and auto-encoding reasons:
		editor.Field{
			View: editor.Input("Name", s, map[string]string{
				"label":       "Имя",
				"type":        "text",
				"placeholder": "Введите имя услуги",
			}),
		},
		editor.Field{
			View: editor.Input("Price", s, map[string]string{
				"label":       "Цена",
				"type":        "text",
				"placeholder": "Введите цену",
			}),
		},
		editor.Field{
			View: editor.Input("Sale", s, map[string]string{
				"label":       "Цена со скидкой (без скидок оставить пустой)",
				"type":        "text",
				"placeholder": "Введите цену",
			}),
		},
		editor.Field{
			View: reference.Select("Category", s, map[string]string{
				"label": "Категория",
			},
				"Категории",
				`{{ .name }}`,
			),
		},
		//
		// Prices
		//
		editor.Field{
			View: []byte(`
					<div class="input-field col s12">
						<h5 class="active">Ценовая Политика
						<span>(cопоставьте по положению)</span>
						</h5>
					</div>
					`),
			},
		editor.Field{
			View: editor.InputRepeater("PriceOption", s, map[string]string{
				"label":       "Описание",
				"type":        "text",
				"placeholder": "Пакет 00",
			}),
		},
		editor.Field{
			View: editor.InputRepeater("PriceValue", s, map[string]string{
				"label":       "Значение цены",
				"type":        "text",
				"placeholder": "8 000",
			}),
		},
		editor.Field{
			View: editor.Richtext("Description", s, map[string]string{
				"label":       "Описание",
				"placeholder": "Введите описание",
			}),
		},
		editor.Field{
			View: editor.File("Image", s, map[string]string{
				"label":       "Изображение",
				"placeholder": "Загрузите изображение",
			}),
		},
		editor.Field{
			View: editor.Select("Hot", s, map[string]string{
				"label": "Блок горячее",
			}, map[string]string{
				// "value": "Display Name",
				"true" : "Да",
				"false" : "Нет", 
			}),
		},
		editor.Field{
			View: editor.Select("Visible", s, map[string]string{
				"label": "Видимость",
			}, map[string]string{
				// "value": "Display Name",
				"true" : "Видимая",
				"false" : "Невидимая", 
			}),
		},
		editor.Field{
			View: editor.Select("Recent", s, map[string]string{
				"label": "Отображать в Популярных",
			}, map[string]string{
				// "value": "Display Name",
				"true" : "Да",
				"false" : "Нет", 
			}),
		},
		editor.Field{
			View: reference.SelectRepeater("Related", s, map[string]string{
				"label": "Связанные услуги",
			},
				"Услуги",
				`{{ .name }}`,
			),
		},
		editor.Field{
			View: reference.SelectRepeater("RelatedProducts", s, map[string]string{
				"label": "Связные продукты",
			},
				"Продукты",
				`{{ .name }}`,
			),
		},
		editor.Field{
			View: editor.Tags("Tags", s, map[string]string{
				"label":       "SEO Ключевые слова",
				"placeholder": "+Tags",
			}),
		},
		editor.Field{
			View: editor.Input("SeoTitle", s, map[string]string{
				"label":       "Seo Заголовок",
				"type":        "text",
				"placeholder": "Введите описание",
			}),
		},
		editor.Field{
			View: editor.Input("SeoDescription", s, map[string]string{
				"label":       "Seo Описание",
				"type":        "text",
				"placeholder": "Введите описание",
			}),
		},
		editor.Field{
			View: editor.File("SeoImage", s, map[string]string{
				"label":       "Изображение SEO",
				"placeholder": "Загрузите изображение",
			}),
		},
	)

	if err != nil {
		return nil, fmt.Errorf("Failed to render Services editor view: %s", err.Error())
	}

	return view, nil
}

func init() {
	item.Types["Услуги"] = func() interface{} { return new(Services) }
}

// String defines how a Services is printed. Update it using more descriptive
// fields from the Services struct type
func (s *Services) String() string {
	return fmt.Sprintf(s.Name)
}
