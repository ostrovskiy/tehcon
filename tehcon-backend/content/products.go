package content

import (
	"fmt"

	"github.com/bosssauce/reference"

	"github.com/ponzu-cms/ponzu/management/editor"
	"github.com/ponzu-cms/ponzu/system/item"
)

type Products struct {
	item.Item

	Name        	  string    `json:"name"`
	Price       	  string    `json:"price"`
	Sale	    	  string 	`json:"sale"`
	Vendor			  string    `json:"vendor"`
	Category    	  string    `json:"category"`
	PriceOption		[]string	`json:"price_option"`
	PriceValue		[]string	`json:"price_value"`
	Description 	  string    `json:"description"`
	Images		  	[]string    `json:"images"`
	SeoTitle 	  	  string    `json:"seo_title"`
	Tags          	[]string    `json:"tags"`
	SeoDescription 	  string    `json:"seo_description"`
	SeoImage       	  string 	`json:"seo_image"`
	Related		  	[]string    `json:"related_products"`
	Hot				  bool 		`json:"hot"`
	Visible     	  bool		`json:"visible"`
	Recent			  bool	    `json:"recent"`
	RelatedServices []string 	`json:"related_services"`
}

// MarshalEditor writes a buffer of html to edit a Products within the CMS
// and implements editor.Editable
func (p *Products) MarshalEditor() ([]byte, error) {
	view, err := editor.Form(p,
		// Take note that the first argument to these Input-like functions
		// is the string version of each Products field, and must follow
		// this pattern for auto-decoding and auto-encoding reasons:
		editor.Field{
			View: editor.Input("Name", p, map[string]string{
				"label":       "Имя",
				"type":        "text",
				"placeholder": "Введите имя",
			}),
		},
		editor.Field{
			View: editor.Input("Price", p, map[string]string{
				"label":       "Цена",
				"type":        "text",
				"placeholder": "Введите цену",
			}),
		},
		editor.Field{
			View: editor.Input("Sale", p, map[string]string{
				"label":       "Цена со скидкой (без скидок оставить пустой)",
				"type":        "text",
				"placeholder": "Введите цену",
			}),
		},
		editor.Field{
			View: editor.Input("Vendor", p, map[string]string{
				"label":       "Артикул",
				"type":        "text",
				"placeholder": "Введите артикул",
			}),
		},
		editor.Field{
			View: reference.Select("Category", p, map[string]string{
				"label": "Категория",
			},
				"Категории",
				`{{ .name }}`,
			),
		},
		//
		// Prices
		//
		editor.Field{
			View: []byte(`
					<div class="input-field col s12">
						<h5 class="active">Ценовая Политика
						<span>(cопоставьте по положению)</span>
						</h5>
					</div>
					`),
			},
		editor.Field{
			View: editor.InputRepeater("PriceOption", p, map[string]string{
				"label":       "Описание",
				"type":        "text",
				"placeholder": "Пакет 00",
			}),
		},
		editor.Field{
			View: editor.InputRepeater("PriceValue", p, map[string]string{
				"label":       "Значение цены",
				"type":        "text",
				"placeholder": "8 000",
			}),
		},
		//
		// Main
		//
		editor.Field{
			View: editor.Richtext("Description", p, map[string]string{
				"label":       "Описание",
				"placeholder": "Введите описание",
			}),
		},
		editor.Field{
			View: editor.FileRepeater("Images", p, map[string]string{
				"label":       "Изображения",
				"placeholder": "Загрузите изображения",
			}),
		},
		editor.Field{
			View: editor.Select("Hot", p, map[string]string{
				"label": "Блок горячее",
			}, map[string]string{
				// "value": "Display Name",
				"true" : "Да",
				"false" : "Нет", 
			}),
		},
		editor.Field{
			View: editor.Select("Visible", p, map[string]string{
				"label": "Видимость",
			}, map[string]string{
				// "value": "Display Name",
				"true" : "Видимая",
				"false" : "Невидимая", 
			}),
		},
		editor.Field{
			View: editor.Select("Recent", p, map[string]string{
				"label": "Отображать в Популярных",
			}, map[string]string{
				// "value": "Display Name",
				"true" : "Да",
				"false" : "Нет", 
			}),
		},
		editor.Field{
			View: reference.SelectRepeater("Related", p, map[string]string{
				"label": "Связанные продукты",
			},
				"Продукты",
				`{{ .category }} / {{ .name }}`,
			),
		},
		editor.Field{
			View: reference.SelectRepeater("RelatedServices", p, map[string]string{
				"label": "Связные услуги",
			},
				"Услуги",
				`{{ .category }} / {{ .name }}`,
			),
		},
		editor.Field{
			View: editor.Tags("Tags", p, map[string]string{
				"label":       "SEO Ключевые слова",
				"placeholder": "+Добавьте теги",
			}),
		},
		editor.Field{
			View: editor.Input("SeoTitle", p, map[string]string{
				"label":       "Seo Заголовок",
				"type":        "text",
				"placeholder": "Введите описание",
			}),
		},
		editor.Field{
			View: editor.Input("SeoDescription", p, map[string]string{
				"label":       "Seo Описание",
				"type":        "text",
				"placeholder": "Введите описание",
			}),
		},
		editor.Field{
			View: editor.File("SeoImage", p, map[string]string{
				"label":       "Изображение SEO",
				"placeholder": "Загрузите изображение",
			}),
		},
	)

	if err != nil {
		return nil, fmt.Errorf("Failed to render Products editor view: %s", err.Error())
	}

	return view, nil
}

func init() {
	item.Types["Продукты"] = func() interface{} { return new(Products) }
}

// String defines how a Products is printed. Update it using more descriptive
// fields from the Products struct type
func (p *Products) String() string {
	return fmt.Sprintf(p.Name)
}
