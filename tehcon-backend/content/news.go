package content

import (
	"fmt"

	"github.com/bosssauce/reference"

	"github.com/ponzu-cms/ponzu/management/editor"
	"github.com/ponzu-cms/ponzu/system/item"
)

type News struct {
	item.Item

	Title    		  string  	`json:"title"`
	Content  		  string  	`json:"content"`
	SeoTitle 	  	  string    `json:"seo_title"`
	SEO      		[]string    `json:"seo"`
	SeoDescription 	  string    `json:"seo_description"`
	SeoImage       	  string 	`json:"seo_image"`
	Hot 	 		  bool 	 	`json:"hot"`
	Visible  		  bool 	 	`json:"visible"`
	Image    	      string  	`json:"images"`
	Url      		  string  	`json:"url"`
	Related  		[]string 	`json:"related"`
}

// MarshalEditor writes a buffer of html to edit a Pages within the CMS
// and implements editor.Editable
func (n *News) MarshalEditor() ([]byte, error) {
	view, err := editor.Form(n,
		// Take note that the first argument to these Input-like functions
		// is the string version of each Pages field, and must follow
		// this pattern for auto-decoding and auto-encoding reasons:
		editor.Field{
			View: editor.Input("Title", n, map[string]string{
				"label":       "Заголовок страницы",
				"type":        "text",
				"placeholder": "Enter the Title here",
			}),
		},
		editor.Field{
			View: editor.File("Image", n, map[string]string{
				"label":       "Изображение",
				"placeholder": "Введите изображение",
			}),
		},
		editor.Field{
			View: editor.Richtext("Content", n, map[string]string{
				"label":       "Содержание",
				"placeholder": "Enter the Content here",
			}),
		},
		editor.Field{
			View: editor.Tags("SEO", n, map[string]string{
				"label":       "SEO Ключевые слова",
				"placeholder": "+Ключевые слова",
			}),
		},
		editor.Field{
			View: editor.Input("SeoTitle", n, map[string]string{
				"label":       "Seo Заголовок",
				"type":        "text",
				"placeholder": "Введите описание",
			}),
		},
		editor.Field{
			View: editor.Input("SeoDescription", n, map[string]string{
				"label":       "Seo Описание",
				"type":        "text",
				"placeholder": "Введите описание",
			}),
		},
		editor.Field{
			View: editor.File("SeoImage", n, map[string]string{
				"label":       "Изображение SEO",
				"placeholder": "Загрузите изображение",
			}),
		},
		editor.Field{
			View: editor.Select("Hot", n, map[string]string{
				"label": "Горячая Новость",
			}, map[string]string{
				// "value": "Display Name",
				"true" : "Да",
				"false" : "Нет", 
			}),
		},
		editor.Field{
			View: editor.Select("Visible", n, map[string]string{
				"label": "Видимость",
			}, map[string]string{
				// "value": "Display Name",
				"true" : "Видимая",
				"false" : "Невидимая",
			}),
		},
		editor.Field{
			View: reference.SelectRepeater("Related", n, map[string]string{
				"label": "Связные Новости",
			},
				"Новости",
				`{{ .title }}`,
			),
		},
		editor.Field{
			View: editor.Input("Url", n, map[string]string{
				"label":       "Url",
				"type":        "text",
				"placeholder": "Введите ссылку на страницу",
			}),
		},
	)

	if err != nil {
		return nil, fmt.Errorf("Failed to render Pages editor view: %s", err.Error())
	}

	return view, nil
}

func init() {
	item.Types["Новости"] = func() interface{} { return new(News) }
}

// String defines how a Pages is printed. Update it using more descriptive
// fields from the Pages struct type
func (n *News) String() string {
	return fmt.Sprintf(n.Title)
}
