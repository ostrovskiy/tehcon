package mailer

import (
	"bytes"
	"fmt"
	"time"
	"mime/quotedprintable"
	"net/smtp"
	"strings"
)

type Sender struct {
	User     string
	Password string
	Server string
	Port   string
	SiteName string
}

// Tutorial In Component GO:
// func (s *Email) AfterAPICreate(res http.ResponseWriter, req *http.Request) error {
// 	data := make(map[string]interface{})
// 	user := make(map[string]interface{})
// 	// SMTPUSER
// 	data["mail_from"] = db.ConfigCache("mail_from")
// 	data["admin_email"] = db.ConfigCache("admin_email")
// 	data["smtp_server"] = db.ConfigCache("smtp_server")
// 	data["smtp_port"] = db.ConfigCache("smtp_port")
// 	data["smtp_password"] = db.ConfigCache("smtp_password")
// 	data["name"] = db.ConfigCache("name")
// 	// USER DATA
// 	user["name"] = s.Name
// 	user["phone"] = s.Phone
// 	user["email"] = s.Email
// 	user["product"] = s.Product
// 	user["package"] = s.Package
// 	user["price"] = s.Price
// 	mailer.SendMsg(data, user)
// 	return nil
// }
// }


func SendMsg(data map[string]interface{}, user map[string]interface{}) {
	currentTime := time.Now()
	// GetSettings()
	sender := NewSender(data["mail_from"].(string), data["smtp_password"].(string),data["smtp_server"].(string), data["smtp_port"].(string),data["name"].(string))

	Receiver := []string{data["admin_email"].(string)}
	//The receiver needs to be in slice as the receive supports multiple receiver
	if len(user["email"].(string)) > 0 {
	Receiver = []string{data["admin_email"].(string), user["email"].(string)}
	}

	Subject := "Заказ: " + user["product"].(string) + "; " + user["package"].(string) + "; " + user["price"].(string)
	// MESSAGE BODY
	message := `<!doctype html>
	<html>
	  <head>
		<meta name="viewport" content="width=device-width">
		<style>
		@media only screen and (max-width: 620px) {
		  table[class=body] h1 {
			font-size: 28px !important;
			margin-bottom: 10px !important;
		  }
		  table[class=body] p,
				table[class=body] ul,
				table[class=body] ol,
				table[class=body] td,
				table[class=body] span,
				table[class=body] a {
			font-size: 16px !important;
		  }
		  table[class=body] .wrapper,
				table[class=body] .article {
			padding: 10px !important;
		  }
		  table[class=body] .content {
			padding: 0 !important;
		  }
		  table[class=body] .container {
			padding: 0 !important;
			width: 100% !important;
		  }
		  table[class=body] .main {
			border-left-width: 0 !important;
			border-radius: 0 !important;
			border-right-width: 0 !important;
		  }
		  table[class=body] .btn table {
			width: 100% !important;
		  }
		  table[class=body] .btn a {
			width: 100% !important;
		  }
		  table[class=body] .img-responsive {
			height: auto !important;
			max-width: 100% !important;
			width: auto !important;
		  }
		}
	
		/* -------------------------------------
			PRESERVE THESE STYLES IN THE HEAD
		------------------------------------- */
		@media all {
			:root {
				color-scheme: light;
				}
		  .ExternalClass {
			width: 100%;
		  }
		  .ExternalClass,
				.ExternalClass p,
				.ExternalClass span,
				.ExternalClass font,
				.ExternalClass td,
				.ExternalClass div {
			line-height: 100%;
		  }
		  .apple-link a {
			color: inherit !important;
			font-family: inherit !important;
			font-size: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
			text-decoration: none !important;
		  }
		  #MessageViewBody a {
			color: inherit;
			text-decoration: none;
			font-size: inherit;
			font-family: inherit;
			font-weight: inherit;
			line-height: inherit;
		  }
		  .btn-primary table td:hover {
			background-color: #34495e !important;
		  }
		  .btn-primary a:hover {
			background-color: #34495e !important;
			border-color: #34495e !important;
		  }
		}
		</style>
	  </head>
	  <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
		<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
		  <tr>
			<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
			<td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
			  <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">
	
				<!-- START CENTERED WHITE CONTAINER -->
				<span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Заказ на сайте ООО ТехКОН.</br></span>
				<table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 0px; box-shadow: 0 5px 15px rgba(0, 0, 0, 0.08);">
	
				  <!-- START MAIN CONTENT AREA -->
				  <tr>
					<td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
					  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
						<tr>
						  <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
							<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><img alt="" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIzLjAuNCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMiIgYmFzZVByb2ZpbGU9InRpbnkiIGlkPSJMb2dvIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIgoJIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgMzMwMCA5MDAiIHhtbDpzcGFjZT0icHJlc2VydmUiPgo8cmVjdCBpZD0iU1FVQVJFIiB4PSIyMTUuMyIgeT0iOTEuNiIgZmlsbD0iI0ZERDgzNSIgc3Ryb2tlPSIjRkZFQjNCIiBzdHJva2Utd2lkdGg9IjQ3IiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHdpZHRoPSI3MTguNCIgaGVpZ2h0PSI3MTguNCIvPgo8cGF0aCBpZD0iS09IIiBmaWxsPSIjQzYyODI4IiBkPSJNMTYzMC43LDY0NS4zbDY1LjgtODguOWw0MS40LTU2LjhsNTMuNC03Mi4ybDE2MC4yLTIxOC4zaDQ0LjlsLTE0My42LDE5NC44bC0xMi44LDE3LjVsMTEuNSwxOC44CglsMTUxLjIsMjUwLjRoLTQyLjNMMTg0Mi4yLDQ5NWwtMjUuMi00MS45bC0yOS4xLDM5LjNsLTQ5LjYsNjcuNWwtNi40LDguNXYxMjIuMmgtMzUuNXYtNzUuMmwtNjUuOCw4OC45Vjc1NmgxNjcuMVY1ODkuOGwxNC4xLTE5LjIKCWwxMDIuNSwxNjkuNmw5LjgsMTUuOGgxOTQuOGwtMzAuMy00OS42bC0xNzAtMjgxLjFsMTY4LjgtMjI5LjRsMzguNC01Mi4xaC0yMDcuMmwtOS44LDEzLjJsLTEyLDE2LjdsLTk5LjEsMTM0LjZsLTY1LjgsODguOQoJbC0zNS41LDQ4LjdWMjA5LjFoMzUuNXYxMjkuNWw2NS44LTg5LjNWMTQzLjhoLTE2Ny4xVjY0NS4zeiBNMjA2OS4xLDQ2MS42bDAuNCwwLjR2Ny43bDAuNCwwLjR2NS4xbDAuNCwwLjR2NC4zbDAuNCwwLjR2Mi4xaDEwMy40CgljNy43LDQzLjksMjYuNiw4MC45LDU2LjgsMTExLjFoMC40YzE4LjUsMTguOCw0MC41LDMzLjcsNjQuNyw0NGMyNC42LDEwLjUsNTEsMTUuOCw3OS4yLDE1LjhjNTQuMSwwLjIsMTA2LTIxLjMsMTQ0LTU5LjgKCWMxOC42LTE4LjYsMzMuNC00MC42LDQzLjgtNjQuN2MxMC43LTI0LjYsMTYtNTEuMSwxNi03OS4zYzAtMjguMi01LjMtNTQuNi0xNi03OS4zYy0xMC40LTI0LjItMjUuMi00Ni4xLTQzLjgtNjQuNwoJYy0zOC0zOC41LTg5LjktNjAuMS0xNDQtNTkuOGMtMjguMiwwLTU0LjYsNS4zLTc5LjIsMTUuOGMtMjQuMywxMC4zLTQ2LjMsMjUuMi02NC43LDQ0aC0wLjRjLTIxLjEsMjEuMS0zNy4zLDQ2LjctNDcuNCw3NC44aDcxLjgKCWM2LjItMTAuNCwxMy42LTIwLjEsMjIuMi0yOC42YzI2LjgtMjcuMSw1OS40LTQwLjYsOTcuOC00MC42YzM4LjIsMCw3MC44LDEzLjUsOTcuOCw0MC42YzI2LjgsMjcuMSw0MC4yLDU5LjcsNDAuMiw5Ny44CgljMCwzOC4yLTEzLjQsNzAuOC00MC4yLDk3LjhjLTI3LjEsMjcuMS01OS43LDQwLjYtOTcuOCw0MC42Yy0zOC40LDAtNzEuMS0xMy41LTk3LjgtNDAuNmMtMjcuMS0yNy4xLTQwLjYtNTkuNy00MC42LTk3LjgKCWMwLTEwLjgsMS4zLTIxLjYsMy44LTMyaC0xMDQuMmM4LjMtNTQuNywzMS0xMDAuNyw2OC40LTEzOGMyMi4yLTIxLjksNDguMy0zOS40LDc2LjktNTEuN2MyOS4xLTEyLjUsNjAuMi0xOC44LDkzLjYtMTguOAoJYzMyLjEtMC4yLDYzLjksNi4yLDkzLjQsMTguOGM1Ny44LDI0LjUsMTAzLjgsNzAuNSwxMjguNCwxMjguMmMxMi41LDI5LjEsMTguOCw2MC4yLDE4LjgsOTMuNmMwLDMzLjMtNi4zLDY0LjUtMTguOCw5My42CgljLTEyLjIsMjguNi0yOS43LDU0LjUtNTEuNyw3Ni41Yy0yMS45LDIyLjItNDgsMzkuOC03Ni43LDUxLjljLTI5LjIsMTIuNC02MC4zLDE4LjYtOTMuNCwxOC42Yy0zMy4zLDAtNjQuNS02LjItOTMuNi0xOC42CgljLTI4LjctMTIuMi01NC45LTI5LjgtNzYuOS01MS45Yy0yNy45LTI3LjktNDgtNjEuNC02MC4yLTEwMC40aC02Ny41YzE0LDU3LjUsNDEuMSwxMDYuNSw4MS42LDE0N2MyOCwyOCw2MSw1MC4zLDk3LjQsNjUuOAoJYzM3LDE2LDc2LjgsMjMuOSwxMTkuMiwyMy45YzQyLjIsMCw4MS44LTgsMTE4LjgtMjMuOWMzNi41LTE1LjYsNjkuNy0zNy45LDk3LjgtNjUuOGMyOC0yOCw1MC4yLTYxLjEsNjUuNi05Ny42CgljMTUuOC0zNy4yLDIzLjctNzYuOCwyMy43LTExOXMtNy45LTgxLjgtMjMuNy0xMTljLTE1LjMtMzYuNS0zNy42LTY5LjYtNjUuNi05Ny42Yy0yOC4yLTI3LjktNjEuNC01MC4yLTk3LjgtNjUuOAoJYy0zNy0xNS45LTc2LjYtMjMuOS0xMTguOC0yMy45Yy00Mi40LDAtODIuMiw4LTExOS4yLDIzLjljLTM2LjQsMTUuNS02OS40LDM3LjgtOTcuNCw2NS44Yy00OS42LDQ5LjgtNzguOSwxMTEuNC04OCwxODQuNnYxLjMKCWwtMC40LDAuNHY0LjNsLTAuNCwwLjR2NS4xbC0wLjQsMC40djguMWgtMC40TDIwNjkuMSw0NjEuNkwyMDY5LjEsNDYxLjZ6IE0yNzMyLjIsMTQzLjh2MzEzLjZjMTkuNC0xOC4yLDQ0LjgtMjguNSw3MS40LTI5LjFoMjkwLjkKCVYyMDkuMWgzNS45djQ4MS41aC0zNS45VjQ2MS4yaC0yOTIuMmMtMTguNSwwLTM1LDYuOC00OS42LDIwLjVjLTEzLjcsMTQuNS0yMC41LDMxLTIwLjUsNDkuNlY3NTZoMTY2LjZWNTYyLjVoLTY1LjR2MTI4LjJoLTM1LjkKCVY1MzEuM2MtMC4yLTIuNCwxLjctNC41LDQuMS00LjdjMC4yLDAsMC40LDAsMC42LDBoMjI2LjRWNzU2aDE2Ny4xVjE0My44aC0xNjcuMXYyMTkuMmgtMjI0LjNjLTIuMywwLTQuNiwwLjEtNi44LDAuNFYyMDkuMWgzNS45CglWMzI3aDY1LjRWMTQzLjhMMjczMi4yLDE0My44TDI3MzIuMiwxNDMuOHoiLz4KPHBhdGggaWQ9IlRFWCIgZmlsbD0iIzBENDdBMSIgZD0iTTQ3Ny40LDI0MS43aC0zNS45di0zMi45aDMxMC4ydjMyLjlINTEyLjl2NTE0aDE2Ny4xVjM0Mi45aC02NS40djM0Ny40aC0zNS45VjMwNy41aDIzOC40VjE0My40CglIMzc1Ljd2MTY0LjFoMTAxLjdWMjQxLjdMNDc3LjQsMjQxLjd6IE04MzcuOCwzMTguNFY1NDZjMTMuNy0xMy40LDMyLjEtMjAuOCw1MS4zLTIwLjhoMTI5LjR2MjMuNUg4ODcuOWMtMTMsMC0yNC43LDQuOS0zNS4xLDE0LjYKCWMtMTAsMTAuNC0xNSwyMi4yLTE1LDM1LjR2MTU2LjloMjU2LjdWNjM4LjVIOTU3LjF2LTE3LjRoLTQ3djY0LjRoMTM3LjZWNzA5SDg4NC41VjU5OC44YzAuMi0wLjksMC42LTEuOCwxLjItMi40CgljMC42LTAuNiwxLjMtMC45LDIuMS0wLjloMTc3LjNWNDc4LjZsLTE1Ny41LTAuOWMtNy43LDAtMTUuNSwwLjQtMjMuMiwxLjJWMzY1aDE2My4zdjIzLjVIOTEwLjF2NjMuOGg0N3YtMTYuOGgxMzcuM1YzMTguNEg4MzcuOAoJTDgzNy44LDMxOC40eiBNMTIzMC42LDcwOWgtMjcuOGw3Ni4zLTEwOS45bC0yOC40LTQxLjJMMTEzOSw3MTlsLTI1LjMsMzYuNkgxMjU1bDctMTAuMWw3My4yLTEwNS42bDQ2LjQsNjYuNQoJYzkuNCwxMywxOC41LDI2LDI3LjUsMzkuMWw3LjMsMTAuMWgxNDEuM2wtMjUuNi0zNi45bC01NC45LTc5bC03MC44LTEwMi4ybDE1MS4xLTIxOS4xaC0xNDEuM2wtMTA4LjksMTU3LjgKCWMxOS4zLDI4LjEsMzguNyw1Ni4xLDU4LDgzLjlMMTQ2OC4zLDcwOWgtMjcuOGwtMTU4LjQtMjI4LjNMMTIwMi44LDM2NWgyNy44bDYyLjYsOTAuNmwyOC40LTQxLjJsLTY2LjUtOTYuMWgtMTQxbDMuMSw0LjYKCWM2My4zLDkyLjIsMTI2LjUsMTg0LjEsMTg5LjgsMjc1LjlMMTIzMC42LDcwOXogTTEzNjMuNiw0NzUuOGw3Ni45LTExMC44aDI3LjhsLTkwLjYsMTMxLjJMMTM2My42LDQ3NS44eiIvPgo8L3N2Zz4K" /></p>
							<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><h2 style="color: #0D47A1;">Поступил заказ:</h2></p>
							<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><b>Имя: </b>` + user["name"].(string) + `</p>
							<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><b>Телефон: </b>` + user["phone"].(string) + `</p>
							<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><b>Продукт: </b>` + user["product"].(string) + `</p>
							<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><b>Пакет: </b>` + user["package"].(string) + `</p>
							<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><b>Цена: </b>` + user["price"].(string) + ` руб</p>
							<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><b>Email: </b>` + user["email"].(string) + `</p>
							<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;">
							  <tbody>
								<tr>
								  <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;">
									<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
									  <tbody>
										<tr>
										  <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #0D47A1; border-radius: 5px; text-align: center;"> <a href="https://it-tehcon.ru/" target="_blank" style="display: inline-block; color: #ffffff; background-color: #0D47A1; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 16px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #0D47A1;">На сайт</a> </td>
										</tr>
									  </tbody>
									</table>
								  </td>
								</tr>
							  </tbody>
							</table>
							<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Заявка оставлена в <b style="color: #0D47A1;">` + currentTime.Format("15:04") + ` ` + currentTime.Format("02-01-2006") +`.</b></p>
							<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Мы свяжемся с вами в течении 10 минут.</p>
							<p style="font-family: sans-serif; font-size: 11px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><i>Письмо доставлено с помощью модуля автоматического ответа ООО ТехКОН.</i></p>
						  </td>
						</tr>
					  </table>
					</td>
				  </tr>
	
				<!-- END MAIN CONTENT AREA -->
				</table>
	
				<!-- START FOOTER -->
				<div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
				  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
					<tr>
					  <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
						<span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">ООО ТехКОН, г Курск, ул. Маяковского, д. 39, тел: +7(4712) 220 720</span>
					  </td>
					</tr>
					<tr>
					  <td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
						Создание и дизайн <a href="http://coldestheart.github.io" style="color: #0D47A1; font-size: 12px; text-align: center; text-decoration: none;">Ilia Ostrovskiy</a>.
					  </td>
					</tr>
				  </table>
				</div>
				<!-- END FOOTER -->
	
			  <!-- END CENTERED WHITE CONTAINER -->
			  </div>
			</td>
			<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
		  </tr>
		</table>
	  </body>
	</html>`
	// Call Function To assign
	bodyMessage := sender.WriteHTMLEmail(Receiver, Subject, message)

	sender.SendMail(Receiver, Subject, bodyMessage)
}

func NewSender(Username, Password, Server, Port, SiteName string) Sender {

	return Sender{Username, Password, Server, Port, SiteName}
}

func (sender Sender) SendMail(Dest []string, Subject, bodyMessage string) {

	msg := bodyMessage

	err := smtp.SendMail(sender.Server + ":" + sender.Port,
		smtp.PlainAuth("", sender.User, sender.Password, sender.Server),
		sender.User, Dest, []byte(msg))

	if err != nil {

		fmt.Printf("smtp error: %s", err)
		return
	}

	fmt.Println("Mail sent successfully! " + strings.Join(Dest, "\n"))
}

func (sender Sender) WriteEmail(dest []string, contentType, subject, bodyMessage string) string {

	header := make(map[string]string)

	receipient := ""

	for _, user := range dest {
		receipient = user + ", " +  receipient
	}
	fmt.Println("\n receipient: ",receipient)
	header["From"] = sender.SiteName
	header["To"] = receipient
	header["Subject"] = subject
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = fmt.Sprintf("%s; charset=\"utf-8\"", contentType)
	header["Content-Transfer-Encoding"] = "quoted-printable"
	header["Content-Disposition"] = "inline"

	message := ""

	for key, value := range header {
		message += fmt.Sprintf("%s: %s\r\n", key, value)
	}

	var encodedMessage bytes.Buffer

	finalMessage := quotedprintable.NewWriter(&encodedMessage)
	finalMessage.Write([]byte(bodyMessage))
	finalMessage.Close()

	message += "\r\n" + encodedMessage.String()

	return message
}

func (sender *Sender) WriteHTMLEmail(dest []string, subject, bodyMessage string) string {

	return sender.WriteEmail(dest, "text/html", subject, bodyMessage)
}

// func (sender *Sender) WritePlainEmail(dest []string, subject, bodyMessage string) string {

// 	return sender.WriteEmail(dest, "text/plain", subject, bodyMessage)
// }