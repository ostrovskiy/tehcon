#!/bin/bash


RED='\033[0;31m'
BLUE='\033[0;96m'
GREEN='\033[0;32m'
MAG='\033[0;35m'
YEL='\033[0;33m'
NC='\033[0m'

clear
sudo printf "\n\n${RED}Start${NC} Deploying script\n\n"
printf "\n\n${RED}Customize${NC} installaion:"
printf "\n\n${BLUE}Please${NC} choose number:"
printf "\n\n${RED}[1]${NC} Install ${GREEN}Full${NC}"
printf "\n\n${RED}[2]${NC} Install ${MAG}Reverse Proxy${NC}"
printf "\n\n${RED}[3]${NC} Install ${BLUE}Backend Only${NC} ( Need Reverse Proxy First. If not installed )"
printf "\n\n${RED}[4]${NC} Install ${BLUE}Frontend Only${NC} ( Need Reverse Proxy First. If not installed )"
printf "\n\n\n\n"

read -p "Enter Number (ex.: 1): " uservar

COUNT=0

while [ $COUNT -lt 1 ]
do
case $uservar in

  1)
    clear
    printf "\n\n${GREEN}Performing${NC} ${MAG}Full${NC} Install \n\n"
    COUNT=1
    printf "\n\n${GREEN}Checking${NC} for ${RED}Purity${NC} \n\n"
    docker stop tehcon-frontend tehcon-backend
    docker rm tehcon-frontend tehcon-backend
    docker rmi tehcon-frontend tehcon-backend
    printf "\n\n${GREEN}Creating${NC} docker ${YEL}Networks${NC} \n\n"
    docker network create internal
    docker network create proxy
    printf "\n\n${GREEN}Setting up${NC} ${RED}Reverse Proxy${NC} containers \n\n"
    docker-compose up -d
    cd tehcon-backend
    printf "\n\n${GREEN}Building${NC} ${BLUE}Backend${NC} container \n\n"
    docker build -t tehcon-backend .
    printf "\n\n${GREEN}Setting up${NC} ${BLUE}Backend${NC} container \n\n"
    docker-compose up -d
    cd ../tehcon-frontend
    printf "\n\n${GREEN}Building${NC} ${MAG}Frontend${NC} container \n\n"
    docker build -t tehcon-frontend .
    printf "\n\n${GREEN}Setting up${NC} ${MAG}Frontend${NC} container \n\n"
    docker-compose up -d
    ;;
  2)
    clear
    printf "\n\n${GREEN}Performing${NC} ${BLUE}Reverse Proxy${NC} Install \n\n"
    COUNT=1
    docker network create internal
    docker network create proxy
    printf "\n\n${GREEN}Setting up${NC} ${BLUE}Reverse Proxy${NC} Containers \n\n"
    docker-compose up -d
    ;;
  3)
    clear
    printf "\n\n${GREEN}Performing${NC} ${MAG}Backend Only${NC} Install \n\n"
    COUNT=1
    printf "\n\n${GREEN}Checking${NC} for ${RED}Purity${NC} \n\n"
    docker stop tehcon-backend
    docker rm tehcon-backend
    docker rmi tehcon-backend
    cd tehcon-backend
    printf "\n\n${GREEN}Building${NC} ${BLUE}Backend${NC} Container \n\n"
    docker build -t tehcon-backend .
    printf "\n\n${GREEN}Setting up${NC} ${BLUE}Backend${NC} Container \n\n"
    docker-compose up -d
    ;;
  4)
    clear
    printf "\n\n${GREEN}Performing${NC} ${MAG}Frontend Only${NC} Install \n\n"
    COUNT=1
    printf "\n\n${GREEN}Checking${NC} for ${RED}Purity${NC} \n\n"
    docker stop tehcon-frontend
    docker rm tehcon-frontend
    docker rmi tehcon-frontend
    cd tehcon-frontend
    printf "\n\n${GREEN}Building${NC} ${MAG}Frontend${NC} Container \n\n"
    docker build -t tehcon-frontend .
    printf "\n\n${GREEN}Setting up${NC} ${MAG}Frontend${NC} Container \n\n"
    docker-compose up -d
    ;;
  *)
    printf "\n${RED}Undefined${NC} Please Choose Number\n\n"
    read -p 'Enter Number (ex.: 1): ' uservar
    ;;
esac
done


printf "\n\n\n\nDo you want ${BLUE}fix permissions(optionally)${NC} for containers volumes? \n\n"
printf "\n\n${RED}[1]${NC} ${MAG}YES${NC}"
printf "\n\n${RED}[2]${NC} ${GREEN}NO${NC}\n\n"
read -p 'Enter Number (ex.: 1): ' fixopt

case $fixopt in
  1)
    docker stop $(docker ps -a -q)
    printf "\n${BLUE}ALL CONTAINERS${NC} WAS ${RED}STOPPED${NC}\n\n"
    sudo chmod -R 775 ~/nginx/tehcon-backend
    sudo chmod -R 775 ~/nginx/tehcon-frontend
    docker start $(docker ps -a -q)
    printf "\n${BLUE}ALL CONTAINERS${NC} IS ${GREEN}RUNING${NC}\n\n"
    ;;
  *)
    printf "\n${RED}Exiting${NC} Installer \n\n${GREEN}Good Luck from${NC} ${MAG}Cold!${NC} \n\n"
    ;;
esac