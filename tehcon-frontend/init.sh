#!/bin/bash

BLUE='\033[0;34m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

printf "\n\n\n${RED}Start${NC} Node\n\n\n"

cd /tehcon-frontend

#Finalize
printf "\n\n\n${GREEN}Installing${NC} Nuxt\n\n\n"
npm i --save

printf "\n\n\n${BLUE}Building${NC} Nuxt\n\n\n"
npm run build
printf "\n\n\n${GREEN}TEXKOH FRONTEND${NC} installed\n\n\n"
npm run start