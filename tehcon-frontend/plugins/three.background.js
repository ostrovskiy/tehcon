/* eslint-disable */
import * as THREE from 'three';
// VARIABLES
// VARIABLES
// VARIABLES
const mycont = document.getElementById( "threejs" );
let camera, scene, raycaster, renderer
const mouse = new THREE.Vector2()
let INTERSECTED
let mobile = false;
const radius = 100
const theta = 0
const boxes = []
const posx = [-150, 110, -60, 60, -110, 150]
const posxm = [-60, 40, -20, 20, -40, 60]
const colors = [ 0x221e0a, 0x1b0606, 0x031838 ]
// CONTROLS
let mouseX = 0
let mouseY = 0
let mouseDelta = 0
const target = new THREE.Vector3()
let windowHalfX = window.innerWidth / 2
let windowHalfY = window.innerHeight / 2
let z, i, o
// End


init()
animate()
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max))
}
function init() {
  camera = new THREE.PerspectiveCamera(
    70,
    window.innerWidth / window.innerHeight,
    1,
    10000
  )
  scene = new THREE.Scene()
  scene.background = new THREE.Color(0xFFFFFF)
  const light = new THREE.AmbientLight( 0xC7C4C4 );
  scene.add(light)
  const geometry = new THREE.BoxBufferGeometry(10, 10, 10)
  let posy = 100
  o = 0
  for (i = 0; i < 300; i++) {
    
    const object = new THREE.Mesh(
      geometry,
      new THREE.MeshLambertMaterial({
        wireframe: true,
      })
    )
    // position
    if (o > 5) {
      o = 0
    }
    if (o < 3) {
      if (window.innerWidth > 600) {
      object.position.x = posx[o] + getRandomInt(7) * 2
      } else {
        object.position.x = posxm[o] + getRandomInt(4) * 2
      }
    } else {
      if (window.innerWidth > 600) {
        object.position.x = posx[o] - getRandomInt(7) * 2
        } else {
          object.position.x = posxm[o] - getRandomInt(4) * 2
        }
      object.position.x = posx[o] - getRandomInt(7) * 2
    }
    object.position.y = posy - 10 - getRandomInt(4) * 5
    posy = object.position.y
    object.position.z = getRandomInt(10)
    o++
    // rotation
    object.rotation.z = Math.random() * 1 * Math.PI
    // scaling
    const scaling = getRandomInt(3)
    object.scale.x = .7 + scaling * 0.4
    object.scale.y = .7 + scaling * 0.4
    object.scale.z = .7 + scaling * 0.4
    scene.add(object)
    boxes.push(object)
  }
  raycaster = new THREE.Raycaster()
  renderer = new THREE.WebGLRenderer( { canvas: mycont, antialias: true } );
  renderer.setPixelRatio(window.devicePixelRatio)
  renderer.setSize(window.innerWidth, window.innerHeight)
  window.addEventListener('resize', onWindowResize, false)
  document.addEventListener('scroll', onscroll, true)
  camera.position.x = radius * Math.sin(THREE.Math.degToRad(theta))
  camera.position.y = radius * Math.sin(THREE.Math.degToRad(theta))
  camera.position.z = radius * Math.cos(THREE.Math.degToRad(theta))
  camera.lookAt(scene.position)
  console.log('ThreeJs Has been Succesfuly Initialized')
}
function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight
  camera.updateProjectionMatrix()
  renderer.setSize(window.innerWidth, window.innerHeight)
  windowHalfX = window.innerWidth / 2
  windowHalfY = window.innerHeight / 2
}
function onDocumentMouseMove(event) {
  mouseX = event.clientX - windowHalfX
  mouseY = event.clientY - windowHalfY
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1
}
function onscroll(event) {
  mouseDelta = window.pageYOffset * 0.07
}

//
function animate() {
  requestAnimationFrame(animate)
  render()
  for (z = 0; z < 299; z++) {
    boxes[z].rotation.z += 0.002
    boxes[z].rotation.y += 0.002
  }
  raycaster.setFromCamera(mouse, camera)
  target.x = (1 - mouseX) * 0.00005
  target.y = (1 - mouseY) * 0.00005
  target.z = 1 - mouseDelta
  camera.rotation.x += 0.05 * (target.y - camera.rotation.x)
  camera.rotation.y += 0.05 * (target.x - camera.rotation.y)
  camera.position.y += 0.08 * (target.z - camera.position.y)
  if (INTERSECTED) {
  INTERSECTED.rotation.y -= 0.01
  }
}
function render() {
  camera.updateMatrixWorld()
  // find intersections
  const intersects = raycaster.intersectObjects(scene.children)
  if (intersects.length > 0) {
    if (INTERSECTED !== intersects[0].object) {
      if (INTERSECTED)
        INTERSECTED.material.emissive.setHex(INTERSECTED.currentHex)
      INTERSECTED = intersects[0].object
      INTERSECTED.currentHex = INTERSECTED.material.emissive.getHex()
      INTERSECTED.material.emissive.setHex(colors[getRandomInt(3)])
    }
  } else {
    if (INTERSECTED)
      INTERSECTED.material.emissive.setHex(INTERSECTED.currentHex)
    INTERSECTED = null
  }
  renderer.render(scene, camera)
}

// MoveChanger
if (window.DeviceOrientationEvent && isMobileDevice()) {
  window.addEventListener('deviceorientation', function(ev) {
    mouseX = ev.gamma * 100
    mouseY = -ev.beta * 80
  });
} else {
  document.addEventListener('mousemove', onDocumentMouseMove, false)
}
function isMobileDevice() {
  return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};
// End