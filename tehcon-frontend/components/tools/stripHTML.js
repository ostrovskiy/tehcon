export default {
  strippedContent(string) {
    return string.replace(/<\/?[^>]+>/gi, ' ')
  }
}
