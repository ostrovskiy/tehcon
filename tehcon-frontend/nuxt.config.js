export default {
  mode: 'universal',
  loading: '~/components/partials/loading.vue',
  env: {
    apiUrl: 'https://api.it-tehcon.ru'
    // apiUrl: 'http://localhost:8080'
  },
  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: {
      lang: 'ru'
      // amp: false
    },
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon/favicon.ico' }]
  },
  server: {
    port: 80, // default: 3000 // standart: 80
    host: '0.0.0.0' // default: localhost
  },
  /*
   ** Global CSS
   */
  css: [
    '@vuikit/theme',
    { src: '~/assets/styles/styles.scss', lang: 'scss' },
    { src: '~/assets/styles/fonts/fontawesome.scss', lang: 'scss' },
    { src: '~/assets/styles/fonts/light.scss', lang: 'scss' },
    { src: '~/assets/styles/fonts/brands.scss', lang: 'scss' }
  ],
  manifest: {
    name: 'Вебсайт ООО Техкон',
    short_name: 'ООО Техкон',
    description:
      'Вебсайт компании ООО Техкон, продажа, разработка, консультирование 1с',
    icons: [],
    start_url: '/?standalone=true',
    display: 'standalone',
    background_color: '#ffffff',
    theme_color: '#fff',
    lang: 'ru'
  },
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/vuikit.js',
    '~/plugins/socialSharing.js',
    { src: '~plugins/googleAnalytics.js', mode: 'client' }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/svg',
    'nuxt-compress',
    'nuxt-seo',
    [
      'nuxt-validate',
      {
        lang: 'ru'
      }
    ],
    [
      '@nuxtjs/yandex-metrika',
      {
        id: '57630619',
        webvisor: true,
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        defer: true
      }
    ]
  ],
  'nuxt-compress': {
    gzip: {
      cache: true
    },
    brotli: {
      threshold: 10240
    }
  },
  /*
   ** Advanced SEO
   */
  sitemap: {
    hostname: 'https://it-tehcon.ru',
    gzip: true,
    changefreq: 'daily'
  },
  robots: {
    UserAgent: '*',
    Disallow: '/assets',
    Allow: '/',
    Host: 'https://it-tehcon.ru'
    // Sitemap: 'https://it-tehcon.ru/sitemap.xml'
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
    postcss: {
      // Add plugin names as key and arguments as value
      // Install them before as dependencies with npm or yarn
      plugins: {
        // Disable a plugin by passing false as value
        'postcss-url': false,
        'postcss-nested': {},
        'postcss-responsive-type': {},
        'postcss-hexrgba': {}
      },
      preset: {
        // Change the postcss-preset-env settings
        autoprefixer: {
          grid: true,
          supports: true,
          flexbox: true,
          remove: true
        }
      }
    }
  }
}
