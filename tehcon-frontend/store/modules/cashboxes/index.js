import Axios from 'axios'

const state = () => ({
  list: [],
  loaded: false
})

const mutations = {
  set_cashboxes(state, payload) {
    payload = payload.filter((obj) => obj.visible === true)
    payload.sort((a, b) => b.timestamp - a.timestamp)
    state.list = payload
    state.loaded = true
  }
}

const getters = {
  // HomePage_Getters
  recent(state) {
    const cashboxes = state.list.filter((cashbox) => cashbox.recent)
    if (!cashboxes) {
      return null
    }
    return cashboxes
  }
}

const actions = {
  async get_cashboxes(context, payload) {
    const { data } = await Axios.get(
      process.env.apiUrl +
        '/api/contents?type=%D0%9A%D0%B0%D1%81%D1%81%D1%8B&count=-1&offset=0'
    )
    // GETTING CATEGORIES From OBJ
    for (let i = 0; i < data.data.length; i++) {
      if (data.data[i].category !== null) {
        await Axios.get(
          process.env.apiUrl + '/api' + encodeURI(data.data[i].category)
        ).then(
          (response) => (data.data[i].category = response.data.data[0].name)
        )
      }
    }
    // END GETTING CATEGORIES From OBJ
    context.commit('set_cashboxes', data.data)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
