import Axios from 'axios'

const actions = {
  async send_message(context, payload) {
    await Axios({
      method: 'post',
      url: process.env.apiUrl + '/api/content/create?type=Emails',
      // url: 'http://localhost:8080/api/content/create?type=Emails',
      data: payload,
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }
}

export default {
  actions
}
