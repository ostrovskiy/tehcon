import Axios from 'axios'

const state = () => ({
  list: [],
  loaded: false
})

const mutations = {
  set_services(state, payload) {
    payload = payload.filter((obj) => obj.visible === true)
    payload.sort((a, b) => b.timestamp - a.timestamp)
    state.list = payload
    state.loaded = true
  }
}

const actions = {
  async get_services(context, payload) {
    const { data } = await Axios.get(
      process.env.apiUrl +
        '/api/contents?type=%D0%A3%D1%81%D0%BB%D1%83%D0%B3%D0%B8&count=-1&offset=0'
    )
    // GETTING CATEGORIES From OBJ
    for (let i = 0; i < data.data.length; i++) {
      if (data.data[i].category !== null) {
        await Axios.get(
          process.env.apiUrl + '/api' + encodeURI(data.data[i].category)
        ).then(
          (response) => (data.data[i].category = response.data.data[0].name)
        )
      }
    }
    // END GETTING CATEGORIES From OBJ
    context.commit('set_services', data.data)
  }
}

export default {
  state,
  mutations,
  actions
}
