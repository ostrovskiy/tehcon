import Axios from 'axios'

const state = () => ({
  data: [],
  error: []
})

const mutations = {
  on_destroy(state) {
    state.data = []
    state.error = []
  },
  set_item(state, payload) {
    payload.timestamp = new Intl.DateTimeFormat('en-GB').format(
      payload.timestamp
    )
    if (payload.related_products) {
      payload.related_products = payload.related_products.filter((item) => item)
    }
    if (payload.related_services) {
      payload.related_services = payload.related_services.filter((item) => item)
    }
    state.data = payload
  },
  set_error(state, payload) {
    state.error = payload
  }
}

const actions = {
  async get_item(context, payload) {
    await context.commit('on_destroy')
    try {
      const { data } = await Axios.get(
        process.env.apiUrl + '/api/content?slug=' + encodeURI(payload)
      )
      await context.dispatch('get_references', data.data[0])
    } catch (err) {
      context.commit('set_error', err)
    }
  },
  // GET REFERENCES
  async get_references(context, payload) {
    if (payload.related_products && payload.related_products !== null) {
      for (let i = 0; i < payload.related_products.length; i++) {
        try {
          const { data } = await Axios.get(
            process.env.apiUrl + '/api' + encodeURI(payload.related_products[i])
          )
          payload.related_products[i] = data.data[0]
        } catch {
          payload.related_products[i] = null
        }
      }
    }
    if (payload.related_products && payload.related_services !== null) {
      for (let i = 0; i < payload.related_services.length; i++) {
        try {
          const { data } = await Axios.get(
            process.env.apiUrl + '/api' + encodeURI(payload.related_services[i])
          )
          payload.related_services[i] = data.data[0]
        } catch {
          payload.related_services[i] = null
        }
      }
    }
    await context.commit('set_item', payload)
  }
}

export default {
  state,
  mutations,
  actions
}
