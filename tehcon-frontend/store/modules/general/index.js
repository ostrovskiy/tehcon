import Axios from 'axios'

const state = () => ({
  info: [],
  pages: [],
  loaded: false
})

const mutations = {
  set_info(state, payload) {
    state.info = payload
  },
  set_pages(state, payload) {
    payload.sort((a, b) => a.order - b.order)
    state.pages = payload
    state.loaded = true
  }
}

const getters = {
  // HomePage_Getters
  HomeAdvantages(state) {
    const page = state.pages.find((page) => page.template === 'home')
    if (!page) {
      return null
    }
    return page.home_advantages
  },
  RecentText(state) {
    const page = state.pages.find((page) => page.template === 'home')
    if (!page) {
      return null
    }
    return page.home_description
  },
  ItcBlock(state) {
    const page = state.pages.find((page) => page.template === 'home')
    if (!page) {
      return null
    }
    const send = {}
    send.price_option = page.home_itc_price_option
    send.price_value = page.home_itc_price_value
    send.image = page.home_itc_image
    send.link = page.home_itc_link
    send.description = page.home_itc_description
    return send
  },
  // AboutGetters
  About(state) {
    const page = state.pages.find((page) => page.template === 'about')
    if (!page) {
      return null
    }
    return page
  },
  AboutLogo(state) {
    if (!state.info) {
      return null
    }
    return state.info.logo
  }
}

const actions = {
  async get_data(context, payload) {
    const { data } = await Axios.get(
      process.env.apiUrl +
        '/api/contents?type=%D0%A1%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D1%8B'
    )
    await context.dispatch('get_info')
    await context.dispatch('get_references', data.data)
    // Call Reference Action
    // context.commit('set_pages', data.data)
  },
  async get_info(context, payload) {
    const { data } = await Axios.get(process.env.apiUrl + '/api/settings')
    context.commit('set_info', data.data)
  },
  // Getting References for pages
  async get_references(context, payload) {
    for (let i = 0; i < payload.length; i++) {
      if (payload[i].subcategories !== null) {
        for (let di = 0; di < payload[i].subcategories.length; di++) {
          // START AXIOS SYNC
          await Axios.get(
            process.env.apiUrl +
              '/api' +
              encodeURI(payload[i].subcategories[di])
          ).then(
            (response) => (payload[i].subcategories[di] = response.data.data[0])
          )
          // Dive to Second
          if (
            payload[i].subcategories[di].subcategories !== null &&
            payload[i].subcategories[di].subcategories !== undefined
          ) {
            for (
              let num = 0;
              num < payload[i].subcategories[di].subcategories.length;
              num++
            ) {
              await Axios.get(
                process.env.apiUrl +
                  '/api' +
                  encodeURI(payload[i].subcategories[di].subcategories[num])
              ).then(
                (response) =>
                  (payload[i].subcategories[di].subcategories[num] =
                    response.data.data[0])
              )
              // DIVE TO Third
              if (
                payload[i].subcategories[di].subcategories[num].subcategories !=
                  null &&
                payload[i].subcategories[di].subcategories[num]
                  .subcategories !== undefined
              ) {
                for (
                  let numb = 0;
                  numb <
                  payload[i].subcategories[di].subcategories[num].subcategories
                    .length;
                  numb++
                ) {
                  await Axios.get(
                    process.env.apiUrl +
                      '/api' +
                      encodeURI(
                        payload[i].subcategories[di].subcategories[num]
                          .subcategories[numb]
                      )
                  ).then(
                    (response) =>
                      (payload[i].subcategories[di].subcategories[
                        num
                      ].subcategories[numb] = response.data.data[0])
                  )
                }
              }
            }
          }
        }
      }
    }
    context.commit('set_pages', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
