import Axios from 'axios'

const state = () => ({
  list: [],
  loaded: false
})

const mutations = {
  set_news(state, payload) {
    payload = payload.filter(
      (obj) => obj.visible === true && obj.timestamp <= new Date().getTime()
    )
    payload.sort((a, b) => b.timestamp - a.timestamp)
    payload.forEach(
      (element) =>
        (element.timestamp = new Intl.DateTimeFormat('en-GB').format(
          element.timestamp
        ))
    )
    state.list = payload
    state.loaded = true
  }
}

const actions = {
  async get_news(context, payload) {
    const { data } = await Axios.get(
      process.env.apiUrl +
        '/api/contents?type=%D0%9D%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D0%B8&count=-1&offset=0'
    )
    context.commit('set_news', data.data)
  }
}

export default {
  state,
  mutations,
  actions
}
