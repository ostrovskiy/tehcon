import Axios from 'axios'

const state = () => ({
  list: [],
  loaded: true
})

const mutations = {
  set_products(state, payload) {
    payload = payload.filter((obj) => obj.visible === true)
    payload.sort((a, b) => b.timestamp - a.timestamp)
    state.list = payload
  }
}

const actions = {
  async get_products(context, payload) {
    const { data } = await Axios.get(
      process.env.apiUrl +
        '/api/contents?type=%D0%9F%D1%80%D0%BE%D0%B4%D1%83%D0%BA%D1%82%D1%8B&count=-1&offset=0'
    )
    // GETTING CATEGORIES From OBJ
    for (let i = 0; i < data.data.length; i++) {
      if (data.data[i].category !== null) {
        await Axios.get(
          process.env.apiUrl + '/api' + encodeURI(data.data[i].category)
        ).then(
          (response) => (data.data[i].category = response.data.data[0].name)
        )
      }
    }
    // END GETTING CATEGORIES From OBJ
    context.commit('set_products', data.data)
  }
}

export default {
  state,
  mutations,
  actions
}
