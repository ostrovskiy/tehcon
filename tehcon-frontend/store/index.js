const actions = {
  async nuxtServerInit(context) {
    await context.dispatch('modules/general/get_data')
  }
}

export default {
  actions
}
